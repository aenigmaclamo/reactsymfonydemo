<?php

namespace SiliconTravel\ItineraryBuilder\GuestListBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GuestListType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email')
            ->add('status')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SiliconTravel\ItineraryBuilder\GuestListBundle\Entity\GuestList'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'silicontravel_itinerarybuilder_guestlistbundle_guestlist';
    }
}

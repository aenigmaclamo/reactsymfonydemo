<?php

namespace SiliconTravel\ItineraryBuilder\GuestListBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use SiliconTravel\ItineraryBuilder\GuestListBundle\Entity\GuestList;
use SiliconTravel\ItineraryBuilder\GuestListBundle\Form\GuestListType;

use Symfony\Component\HttpFoundation\Response;


/**
 * GuestList controller.
 *
 * @Route("/guestlist")
 */
class GuestListController extends Controller
{

    /**
     * Lists all GuestList entities.
     *
     * @Route("/", name="guestlist")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SiliconTravelItineraryBuilderGuestListBundle:GuestList')->findAll();


			//	if($request->isXmlHttpRequest()) {
				if(true){
					$response = new Response(json_encode(array('entities'=>$entities)));
					$response->headers->set('Content-Type', 'application/json');

					return $response;
				} else {
					return array(
							'entities' => $entities,
					);
				}
    }
    /**
     * Creates a new GuestList entity.
     *
     * @Route("/", name="guestlist_create")
     * @Method("POST")
     * @Template("SiliconTravelItineraryBuilderGuestListBundle:GuestList:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new GuestList();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('guestlist_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a GuestList entity.
     *
     * @param GuestList $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(GuestList $entity)
    {
        $form = $this->createForm(new GuestListType(), $entity, array(
            'action' => $this->generateUrl('guestlist_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new GuestList entity.
     *
     * @Route("/new", name="guestlist_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new GuestList();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a GuestList entity.
     *
     * @Route("/{id}", name="guestlist_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SiliconTravelItineraryBuilderGuestListBundle:GuestList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GuestList entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing GuestList entity.
     *
     * @Route("/{id}/edit", name="guestlist_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SiliconTravelItineraryBuilderGuestListBundle:GuestList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GuestList entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a GuestList entity.
    *
    * @param GuestList $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GuestList $entity)
    {
        $form = $this->createForm(new GuestListType(), $entity, array(
            'action' => $this->generateUrl('guestlist_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing GuestList entity.
     *
     * @Route("/{id}", name="guestlist_update")
     * @Method("PUT")
     * @Template("SiliconTravelItineraryBuilderGuestListBundle:GuestList:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SiliconTravelItineraryBuilderGuestListBundle:GuestList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GuestList entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('guestlist_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a GuestList entity.
     *
     * @Route("/{id}", name="guestlist_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SiliconTravelItineraryBuilderGuestListBundle:GuestList')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GuestList entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('guestlist'));
    }

    /**
     * Creates a form to delete a GuestList entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('guestlist_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

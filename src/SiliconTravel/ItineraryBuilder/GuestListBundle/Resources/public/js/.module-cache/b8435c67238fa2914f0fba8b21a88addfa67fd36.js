/** represents table of GuestListItem components */
var GuestList = React.createClass({displayName: "GuestList",
  /**
   * Unused. Can be used for input validation of properties.
   */
  propTypes: {
    /*
    count: function(props, propName) {
        if (typeof props[propName] !== "number") {
          return new Error('The count property must be a number!');
        }

        if (props[propName] > this.props.maxPeople) {
          return new Error('Count cannot exceed: ' + this.props.maxPeople);
        }
    }
    */
  },
  /**
   * Gets the initial state.
   */
  getInitialState: function() {
    return {
      currentKey: 0,
      guests: []
    };
  },
  /**
   * This is run right before the first time the component is rendered.
   */
  componentDidMount: function() {
    this.loadFromServer();
  },
  /**
   * My utility method to download content from the server. Taken from what I
   * wrote for my Spring demo.
   */
  loadFromServer: function() {
    var self = this;
    var req = new XMLHttpRequest();
    req.onload = function () {
      var rsp = req.response;

      console.table(rsp);
      if(rsp) {
        self.setState({guests: rsp.entities});
      }
    };

    req.open("GET", "http://127.0.0.1:8000/guestlist/", true);
    req.responseType = "json";
    req.send();
  },

  /**
   * TODO: make method to save to server
   */
  saveToServer: function() {

  },
  /**
   * Adds a new guest to the guest list. Pushes an empty object to be populated
   * by the GuestListItem component later.
   *
   * Note that it will stay being an empty object until the save button is
   * clicked in the GuestListItem component.
   */
  add: function() {
    var tmpGuests = this.state.guests;
    tmpGuests.push({});
    this.setState({guests: tmpGuests});
  },
  /**
   * Updates t
   */
  update: function(newObj, i) {
    var arr = this.state.guests;
    arr[i] = newObj;
    this.setState({guests: arr});
  },
  remove: function(index) {
    var arr = this.state.guests;
    arr.splice(index, 1);
    this.setState({guests: arr});
  },
  eachGuest: function(guest, i) {
    var guest = this.state.guests[i];
    return (
      React.createElement(GuestListItem, {
        key: i, 
        index: i, 
        onChange: this.update, 
        onRemove: this.remove, 
        name: guest.name, 
        email: guest.email, 
        status: guest.status}
      )
    );
  },
  render: function() {
    console.table(this.state.guests);
    return (
    React.createElement("div", null, 
      React.createElement("table", {className: "table table-hover table-condensed"}, 
      React.createElement("thead", null, 
        React.createElement("tr", null, 
          React.createElement("th", null, "Name"), 
          React.createElement("th", null, "Email"), 
          React.createElement("th", null, "Attending?"), 
          React.createElement("th", null)
        )
      ), 
      React.createElement("tbody", null, 
        this.state.guests.map(this.eachGuest)
      )
      ), 
      React.createElement("button", {onClick: this.add, 
      className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

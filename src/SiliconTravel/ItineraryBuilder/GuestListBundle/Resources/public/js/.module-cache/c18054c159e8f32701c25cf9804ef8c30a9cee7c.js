React.render(
  React.createElement(GuestList, {parentNodeClassName: "list-group", 
    childNodeClassName: "list-group-item"}
  ),
  document.getElementById('guestlist-react-container')
);

var GuestList = React.createClass({displayName: "GuestList",
  render: function() {
    return (
    React.createElement("ul", {className: "{this.props.parentNodeClassName}"}, 
      this.props.children
    )
  );}
});

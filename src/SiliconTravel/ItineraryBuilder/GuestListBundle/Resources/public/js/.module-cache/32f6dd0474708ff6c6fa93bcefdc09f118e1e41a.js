var GuestListItem = React.createClass({displayName: "GuestListItem",
  render: function() {
    return(
      React.createElement("li", {className: "{this.props.className}"}, 
      React.createElement("button", {className: "btn glyphicon glyphicon-edit"}), 
      React.createElement("button", {className: "btn glyphicon glyphicon-trash"})
      )
    );
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  render: function() {
    return (
    React.createElement("div", null, 
      React.createElement("ul", {className: "{this.props.parentNodeClassName}"}, 
        this.props.children
      ), 
      React.createElement("button", {className: "btn btn-primary glyphicon glyphicon-plus"}, "Add New Guest")
    )
  );}
});

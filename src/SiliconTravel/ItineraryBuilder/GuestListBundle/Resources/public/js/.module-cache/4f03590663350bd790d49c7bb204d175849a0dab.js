var GuestListItem = React.createClass({displayName: "GuestListItem",

  /**
   * Default properties are set with this.
   */
  getDefaultProps: function() {
    return {
      name: "Name",
      email: "EmailAddress@email.com",
      status: 0
    }
  },
  getInitialState: function() {
    return {editMode: false};
  },
  decodeStatus: function(code) {
    return ["Attending", "Maybe", "Not Attending"][code];
  },
  encodeStatus: function(statusText) {
    var map = {
      "attending": 0,
      "maybe": 1,
      "not attending":2
    }

    return map[statusText.toLowerCase()]
  },
  save: function() {
    var name = this.refs.name.getDOMNode().value;
    var email = this.refs.email.getDOMNode().value;
    var status = parseInt(this.refs.status.getDOMNode().value);

    this.setState({editMode: false});

    this.props.onChange({
      name: name,
      email: email,
      status: status
    }, this.props.index);
  },
  edit: function() {
    this.setState({editMode: true});
  },

  remove: function() {
    this.props.onRemove(this.props.index);
  },
  renderDisplay() {
    return(
      React.createElement("tr", null, 
        React.createElement("td", null, this.props.name), 
        React.createElement("td", null, this.props.email), 
        React.createElement("td", null, this.decodeStatus(this.props.status)), 

        React.createElement("td", {className: "text-right"}, 
        React.createElement("span", {className: "showOnHover"}, 
          React.createElement("button", {onClick: this.edit, 
                  className: "btn btn-default glyphicon glyphicon-edit"}), 
          " ", 
          React.createElement("button", {onClick: this.remove, 
                  className: "btn btn-danger glyphicon glyphicon-trash"})
        )
        )
      )
    );
  },
  renderForm() {
    return (
      React.createElement("tr", null, 
        React.createElement("td", null, 
          React.createElement("input", {ref: "name", 
                className: "form-control", 
                type: "text", 
                defaultValue: this.props.name})
        ), 
        React.createElement("td", null, 
          React.createElement("input", {ref: "email", className: "form-control", type: "email", defaultValue: this.props.email})
        ), 
        React.createElement("td", null, 
          React.createElement("select", {ref: "status", className: "form-control", defaultValue: this.props.status}, 
            React.createElement("option", {value: "0"}, this.decodeStatus(0)), 
            React.createElement("option", {value: "1"}, this.decodeStatus(1)), 
            React.createElement("option", {value: "2"}, this.decodeStatus(2))
          )
        ), 

        React.createElement("td", {className: "text-right"}, 
          React.createElement("button", {onClick: this.save, 
                  className: "btn btn-primary glyphicon glyphicon-floppy-save"}
          )
        )
      )
    );
  },
  render: function() {
    if(this.state.editMode) {
      return this.renderForm();
    } else {
      return this.renderDisplay();
    }
  }
});

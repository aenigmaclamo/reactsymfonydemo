var GuestListItem = React.createClass({displayName: "GuestListItem",
  getInitialState: function() {
    return {
      name: "Name",
      email: "Email",
      status: 0
    };
  },
  edit: function() {
    console.log("editing!")
  },

  remove: function() {
    console.log("deleting!")
  },
  render: function() {
    return(
      React.createElement("tr", null, 
        React.createElement("td", null, this.state.name), 
        React.createElement("td", null, this.state.email), 
        React.createElement("td", null, this.state.status), 

        React.createElement("td", {className: "text-right"}, 
        React.createElement("span", {className: "showOnHover"}, 
          React.createElement("button", {onClick: this.edit, 
                  className: "btn glyphicon glyphicon-edit"}), 
          " ", 
          React.createElement("button", {onClick: this.remove, 
                  className: "btn glyphicon glyphicon-trash"})
        )
        )
      )
    );
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  getInitialState: function() {
    return {};
  },
  render: function() {
    return (
    React.createElement("div", null, 
      React.createElement("table", {className: "table table-hover table-condensed"}, 
      React.createElement("thead", null
      
      ), 
      React.createElement("tbody", null, 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName}), 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName})
      )
      ), 
      React.createElement("button", {className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

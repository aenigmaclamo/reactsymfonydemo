var GuestListItem = React.createClass({displayName: "GuestListItem",

  getDefaultProps: function() {
    return {
      name: "Name",
      email: "EmailAddress@email.com",
      status: 0
    }
  },
  getInitialState: function() {
    return {editMode: false};
  },
  decodeStatus: function(code) {
    return ["Attending", "Maybe", "Not Attending"][code];
  },
  encodeStatus: function(statusText) {
    var map = {
      "attending": 0,
      "maybe": 1,
      "not attending":2
    }

    return map[statusText.toLowerCase()]
  },
  save: function() {
    var name = this.refs.name.getDOMNode().value;
    var email = this.refs.email.getDOMNode().value;
    var status = parseInt(this.refs.status.getDOMNode().value);

    this.setState({editMode: false});

    this.props.onChange({
      name: name,
      email: email,
      status: status
    }, this.props.index);
  },
  edit: function() {
    this.setState({editMode: true});
  },

  remove: function() {
    this.props.onRemove(this.props.index);
  },
  renderDisplay() {
    return(
      React.createElement("tr", null, 
        React.createElement("td", null, this.props.name), 
        React.createElement("td", null, this.props.email), 
        React.createElement("td", null, this.decodeStatus(this.props.status)), 

        React.createElement("td", {className: "text-right"}, 
        React.createElement("span", {className: "showOnHover"}, 
          React.createElement("button", {onClick: this.edit, 
                  className: "btn btn-default glyphicon glyphicon-edit"}), 
          " ", 
          React.createElement("button", {onClick: this.remove, 
                  className: "btn btn-danger glyphicon glyphicon-trash"})
        )
        )
      )
    );
  },
  renderForm() {
    return (
      React.createElement("tr", null, 
        React.createElement("td", null, 
          React.createElement("input", {ref: "name", 
                className: "form-control", 
                type: "text", 
                defaultValue: this.props.name})
        ), 
        React.createElement("td", null, 
          React.createElement("input", {ref: "email", className: "form-control", type: "email", defaultValue: this.props.email})
        ), 
        React.createElement("td", null, 
          React.createElement("select", {ref: "status", className: "form-control", defaultValue: this.props.status}, 
            React.createElement("option", {value: "0"}, this.decodeStatus(0)), 
            React.createElement("option", {value: "1"}, this.decodeStatus(1)), 
            React.createElement("option", {value: "2"}, this.decodeStatus(2))
          )
        ), 

        React.createElement("td", {className: "text-right"}, 
          React.createElement("button", {onClick: this.save, 
                  className: "btn btn-primary glyphicon glyphicon-floppy-save"}
          )
        )
      )
    );
  },
  render: function() {
    if(this.state.editMode) {
      return this.renderForm();
    } else {
      return this.renderDisplay();
    }
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  propTypes: {
    /*
    count: function(props, propName) {
        if (typeof props[propName] !== "number") {
          return new Error('The count property must be a number!');
        }

        if (props[propName] > this.props.maxPeople) {
          return new Error('Count cannot exceed: ' + this.props.maxPeople);
        }
    }
    */
  },
  getInitialState: function() {
    return {
      guests: []
    };
  },
  componentDidMount:function() {
    var self = this;
    var req = new XMLHttpRequest();
    req.onload = function () {
      var rsp = req.response;

      if(rsp) {
        self.setState({guests: rsp});
      }
    };

    req.open("GET", "guests.json", true);
    req.responseType = "json";
    req.send();
  },
  add: function() {
    var tmpGuests = this.state.guests;
    tmpGuests.push({});
    this.setState({guests: tmpGuests});
  },
  update: function(newObj, i) {
    var arr = this.state.guests;
    arr[i] = newObj;
    this.setState({guests: arr});
  },
  remove: function(index) {
    var arr = this.state.guests;
    arr.splice(index, 1);
    this.setState({guests: arr});
  },
  eachGuest: function(guest, i) {
    var guest = this.state.guests[i];
    return (
      React.createElement(GuestListItem, {
        key: i, 
        index: i, 
        onChange: this.update, 
        onRemove: this.remove, 
        name: guest.name, 
        email: guest.email, 
        status: guest.status}
      )
    );
  },
  render: function() {
    return (
    React.createElement("div", null, 
      React.createElement("table", {className: "table table-hover table-condensed"}, 
      React.createElement("thead", null, 
        React.createElement("tr", null, 
          React.createElement("th", null, "Name"), 
          React.createElement("th", null, "Email"), 
          React.createElement("th", null, "Attending?"), 
          React.createElement("th", null)
        )
      ), 
      React.createElement("tbody", null, 
        this.state.guests.map(this.eachGuest)
      )
      ), 
      React.createElement("button", {onClick: this.add, 
      className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

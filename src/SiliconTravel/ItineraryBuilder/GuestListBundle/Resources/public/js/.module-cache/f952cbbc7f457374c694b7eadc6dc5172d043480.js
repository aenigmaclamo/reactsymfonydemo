var GuestListItem = React.createClass({displayName: "GuestListItem",
  edit: function() {
    console.log("editing!")
  },

  delete: function() {
    console.log("deleting!")
  },
  render: function() {
    return(
      React.createElement("li", {className: this.props.className + " GuestListItem"}, 
        React.createElement("span", null, "foo bar"), 
        React.createElement("span", {className: "pull-right showOnHover", style: "margin-top: -7px;"}, 
          React.createElement("button", {className: "btn glyphicon glyphicon-edit"}), 
          React.createElement("button", {className: "btn glyphicon glyphicon-trash"})
        )
      )
    );
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  defaultItem: function() {
    return (
      React.createElement(GuestListItem, {className: this.props.childNodeClassName})
    );
  },
  render: function() {
    return (
    React.createElement("div", null, 
      React.createElement("ul", {className: this.props.parentNodeClassName}, 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName}), 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName})
      ), 
      React.createElement("button", {className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

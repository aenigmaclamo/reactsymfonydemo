var GuestListItem = React.createClass({displayName: "GuestListItem",
  render: function() {
    return(
      React.createElement("li", {className: "{this.props.className}"}
      
      )
    );
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  render: function() {
    return (
    React.createElement("ul", {className: "{this.props.parentNodeClassName}"}, 
      this.props.children
    )
  );}
});

var GuestListItem = React.createClass({displayName: "GuestListItem",
  render: function() {
    return(
      React.createElement("li", {className: this.props.className}, 
      React.createElement("span", {className: "pull-right"}, 
        React.createElement("button", {className: "btn glyphicon glyphicon-edit"}), 
        React.createElement("button", {className: "btn glyphicon glyphicon-trash"})
      )
      )
    );
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  render: function() {
    return (
    React.createElement("div", null, 
      React.createElement("ul", {className: this.props.parentNodeClassName}
      ), 
      React.createElement("button", {className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

var GuestListItem = React.createClass({displayName: "GuestListItem",
  getInitialState: function() {
    return {
      editMode: false,
      name: "Name",
      email: "Email",
      status: 0
    };
  },
  decodeStatus: function(code) {
    return ["Attending", "Maybe", "Not Attending"][code];
  },
  encodeStatus: function(statusText) {
    var map = {
      "attending": 0,
      "maybe": 1,
      "not attending":2
    }

    return map[statusText.toLowerCase()]
  },
  save: function() {
    console.log("saving!");
  },
  edit: function() {
    console.log("editing!")
  },

  remove: function() {
    console.log("deleting!")
  },
  renderDisplay() {
    return(
      React.createElement("tr", null, 
        React.createElement("td", null, this.state.name), 
        React.createElement("td", null, this.state.email), 
        React.createElement("td", null, this.decodeStatus(this.state.status)), 

        React.createElement("td", {className: "text-right"}, 
        React.createElement("span", {className: "showOnHover"}, 
          React.createElement("button", {onClick: this.edit, 
                  className: "btn glyphicon glyphicon-edit"}), 
          " ", 
          React.createElement("button", {onClick: this.remove, 
                  className: "btn glyphicon glyphicon-trash"})
        )
        )
      )
    );
  },
  renderForm() {
    return (
      React.createElement("tr", null, 
        React.createElement("td", null, 
          React.createElement("input", {className: "form-control", type: "text", value: this.state.name})
        ), 
        React.createElement("td", null, 
          React.createElement("input", {className: "form-control", type: "email", value: this.state.emaill})
        ), 
        React.createElement("td", null, 
          React.createElement("select", {class: "form-control"}, 
            React.createElement("option", {seleted: this.state.status === 0}, this.encodeStatus(0)), 
            React.createElement("option", {seleted: this.state.status === 1}, this.encodeStatus(1)), 
            React.createElement("option", {seleted: this.state.status === 2}, this.encodeStatus(2))
          )
        ), 

        React.createElement("td", {className: "text-right"}, 
        React.createElement("span", {className: "showOnHover"}, 
          React.createElement("button", {onClick: this.edit, 
                  className: "btn glyphicon glyphicon-edit"}), 
          " ", 
          React.createElement("button", {onClick: this.remove, 
                  className: "btn glyphicon glyphicon-trash"})
        )
        )
      )
    );
  },
  render: function() {
    return this.renderDisplay();
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  getInitialState: function() {
    return {};
  },
  render: function() {
    return (
    React.createElement("div", null, 
      React.createElement("table", {className: "table table-hover table-condensed"}, 
      React.createElement("thead", null, 
        React.createElement("tr", null, 
          React.createElement("th", null, "Name"), 
          React.createElement("th", null, "Email"), 
          React.createElement("th", null, "Attending?"), 
          React.createElement("th", null)
        )
      ), 
      React.createElement("tbody", null, 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName}), 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName})
      )
      ), 
      React.createElement("button", {className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

React.render(
  React.createElement("div", null, 
  React.createElement(MyComponent, {text: "Hello World"}, 
  "Hello World Body"
  ), 
  React.createElement(MyComponent, {text: "How Are you?"}), 
  React.createElement(MyComponent, {text: "GoodBye!"})
  ),
document.getElementById('react-container'));

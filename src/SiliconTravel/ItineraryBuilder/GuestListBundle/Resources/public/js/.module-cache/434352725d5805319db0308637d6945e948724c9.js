var GuestListItem = React.createClass({displayName: "GuestListItem",
  getInitialState: function() {
    return {};
  },
  edit: function() {
    console.log("editing!")
  },

  remove: function() {
    console.log("deleting!")
  },
  render: function() {
    return(
      React.createElement("li", {className: this.props.className + " GuestListItem"}, 
        React.createElement("span", null, "foo bar"), 
        React.createElement("span", {className: "pull-right showOnHover", style: {marginTop: -7 + "px"}}, 
          React.createElement("button", {onClick: this.edit, 
                  className: "btn glyphicon glyphicon-edit"}), 
          " ", 
          React.createElement("button", {onClick: this.remove, 
                  className: "btn glyphicon glyphicon-trash"})
        )
      )
    );
  }
});

var GuestList = React.createClass({displayName: "GuestList",
  getInitialState: function() {
    return {};
  },
  render: function() {
    return (
    React.createElement("div", null, 
      React.createElement("ul", {className: this.props.parentNodeClassName}, 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName}), 
        React.createElement(GuestListItem, {className: this.props.childNodeClassName})
      ), 
      React.createElement("button", {className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

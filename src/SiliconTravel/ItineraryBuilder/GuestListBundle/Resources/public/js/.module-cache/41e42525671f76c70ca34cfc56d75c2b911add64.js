var GuestList = React.createClass({displayName: "GuestList",
  propTypes: {
    /*
    count: function(props, propName) {
        if (typeof props[propName] !== "number") {
          return new Error('The count property must be a number!');
        }

        if (props[propName] > this.props.maxPeople) {
          return new Error('Count cannot exceed: ' + this.props.maxPeople);
        }
    }
    */
  },
  getInitialState: function() {
    return {
      guests: []
    };
  },
  componentDidMount: function() {
    this.loadFromServer();
  },
  loadFromServer: function() {
    var self = this;
    var req = new XMLHttpRequest();
    req.onload = function () {
      var rsp = req.response;

      if(rsp) {
        self.setState({guests: rsp});
      }
    };

    req.open("GET", "http://127.0.0.1:8000/guestlist/", true);
    req.responseType = "json";
    req.send();
  },

  saveToServer: function() {

  },
  add: function() {
    var tmpGuests = this.state.guests;
    tmpGuests.push({});
    this.setState({guests: tmpGuests});
  },
  update: function(newObj, i) {
    var arr = this.state.guests;
    arr[i] = newObj;
    this.setState({guests: arr});
  },
  remove: function(index) {
    var arr = this.state.guests;
    arr.splice(index, 1);
    this.setState({guests: arr});
  },
  eachGuest: function(guest, i) {
    var guest = this.state.guests[i];
    return (
      React.createElement(GuestListItem, {
        key: i, 
        index: i, 
        onChange: this.update, 
        onRemove: this.remove, 
        name: guest.name, 
        email: guest.email, 
        status: guest.status}
      )
    );
  },
  render: function() {
    console.table(this.state.guests);
    return (
    React.createElement("div", null, 
      React.createElement("table", {className: "table table-hover table-condensed"}, 
      React.createElement("thead", null, 
        React.createElement("tr", null, 
          React.createElement("th", null, "Name"), 
          React.createElement("th", null, "Email"), 
          React.createElement("th", null, "Attending?"), 
          React.createElement("th", null)
        )
      ), 
      React.createElement("tbody", null, 
        this.state.guests.map(this.eachGuest)
      )
      ), 
      React.createElement("button", {onClick: this.add, 
      className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("button", {onClick: this.add, 
      className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " ")
      ), 
      React.createElement("button", {onClick: this.add, 
      className: "btn btn-primary"}, 
        React.createElement("span", {className: "glyphicon glyphicon-plus"}), 
        React.createElement("span", null, " Add New Guest")
      ), 
      React.createElement("p", null, 
        this.props.children
      )
    )
  );}
});

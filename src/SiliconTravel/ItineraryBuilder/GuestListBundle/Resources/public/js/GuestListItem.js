/** This class is the individual rows in the GuestList */
var GuestListItem = React.createClass({displayName: "GuestListItem",
  /**
   * Default properties are set with this.
   */
  getDefaultProps: function() {
    return {
      name: "Name",
      email: "EmailAddress@email.com",
      status: 0
    }
  },

  /**
   * The initial state is set with this.
   * Edit mode is set to false.
   */
  getInitialState: function() {
    return {editMode: false};
  },
  /**
   * Whether they are attending or not is specified by a number, 0 to 2.
   * They are mapped to their string equivalents here to emulate an enum.
   */
  decodeStatus: function(code) {
    return ["Attending", "Maybe", "Not Attending"][code];
  },
  /**
   * Unused. Can be used to encode from string to the number.
   */
  encodeStatus: function(statusText) {
    var map = {
      "attending": 0,
      "maybe": 1,
      "not attending":2
    }

    return map[statusText.toLowerCase()]
  },
  /**
   * Saves the content by reading the values out of the edit fields and sends it
   * to the associated onChange function to write the change. Next time this
   * component is created with the same object, it will have different
   * properties.
   */
  save: function() {
    var name = this.refs.name.getDOMNode().value;
    var email = this.refs.email.getDOMNode().value;
    var status = parseInt(this.refs.status.getDOMNode().value);

    this.setState({editMode: false});

    this.props.onChange({
      name: name,
      email: email,
      status: status
    }, this.props.index);
  },
  /**
   * Modifies state to edit mode. Next time the component is rendered, it will
   * have text fields and such and a save button.
   */
  edit: function() {
    this.setState({editMode: true});
  },

  /**
   * Calls the onRemove function. Next time the table is rendered, this
   * component won't be rendered as its associated object will be removed.
   */
  remove: function() {
    this.props.onRemove(this.props.index);
  },
  /**
   * Utility method to display the component for just dislpaying. It does not
   * have the text fields to modify its contents. However, edit and remove
   * buttons are available from here.
   */
  renderDisplay() {
    return(
      React.createElement("tr", null, 
        React.createElement("td", null, this.props.name), 
        React.createElement("td", null, this.props.email), 
        React.createElement("td", null, this.decodeStatus(this.props.status)), 

        React.createElement("td", {className: "text-right"}, 
        React.createElement("span", {className: "showOnHover"}, 
          React.createElement("button", {onClick: this.edit, 
                  className: "btn btn-default glyphicon glyphicon-edit"}), 
          " ", 
          React.createElement("button", {onClick: this.remove, 
                  className: "btn btn-danger glyphicon glyphicon-trash"})
        )
        )
      )
    );
  },
  /**
   * Utility method to display the component for editing. It's got text fields
   * to modify the content and a save button.
   */
  renderForm() {
    return (
      React.createElement("tr", null, 
        React.createElement("td", null, 
          React.createElement("input", {ref: "name", 
                className: "form-control", 
                type: "text", 
                defaultValue: this.props.name})
        ), 
        React.createElement("td", null, 
          React.createElement("input", {ref: "email", className: "form-control", type: "email", defaultValue: this.props.email})
        ), 
        React.createElement("td", null, 
          React.createElement("select", {ref: "status", className: "form-control", defaultValue: this.props.status}, 
            React.createElement("option", {value: "0"}, this.decodeStatus(0)), 
            React.createElement("option", {value: "1"}, this.decodeStatus(1)), 
            React.createElement("option", {value: "2"}, this.decodeStatus(2))
          )
        ), 

        React.createElement("td", {className: "text-right"}, 
          React.createElement("button", {onClick: this.save, 
                  className: "btn btn-primary glyphicon glyphicon-floppy-save"}
          )
        )
      )
    );
  },
  /**
   * Depending on whether the component is in edit mode or not, renders using
   * this.renderDisplay or this.renderForm.
   */
  render: function() {
    if(this.state.editMode) {
      return this.renderForm();
    } else {
      return this.renderDisplay();
    }
  }
});

/** represents table of GuestListItem components */
var GuestList = React.createClass({
  /**
   * Unused. Can be used for input validation of properties.
   */
  propTypes: {
    /*
    count: function(props, propName) {
        if (typeof props[propName] !== "number") {
          return new Error('The count property must be a number!');
        }

        if (props[propName] > this.props.maxPeople) {
          return new Error('Count cannot exceed: ' + this.props.maxPeople);
        }
    }
    */
  },
  /**
   * Gets the initial state.
   */
  getInitialState: function() {
    return {
      currentKey: 0,
      guests: []
    };
  },
  /**
   * This is run right before the first time the component is rendered.
   */
  componentDidMount: function() {
    this.loadFromServer();
  },
  /**
   * My utility method to download content from the server. Taken from what I
   * wrote for my Spring demo.
   */
  loadFromServer: function() {
    var self = this;
    var req = new XMLHttpRequest();
    req.onload = function () {
      var rsp = req.response;

      console.table(rsp);
      if(rsp) {
        self.setState({guests: rsp.entities});
      }
    };

    req.open("GET", "http://127.0.0.1:8000/guestlist/", true);
    req.responseType = "json";
    req.send();
  },

  /**
   * TODO: make method to save to server
   */
  saveToServer: function() {

  },
  /**
   * Adds a new guest to the guest list. Pushes an empty object to be populated
   * by the GuestListItem component later.
   *
   * Note that it will stay being an empty object until the save button is
   * clicked in the GuestListItem component.
   */
  add: function() {
    var tmpGuests = this.state.guests;
    tmpGuests.push({});
    this.setState({guests: tmpGuests});
  },
  /**
   * Updates the value of the object at the given index, i.
   *
   * @param {object} newObj - The updated value of the component
   * @param {number} i - index in the internal array to find the
   *                     old object representation
   */
  update: function(newObj, i) {
    var arr = this.state.guests;
    arr[i] = newObj;
    this.setState({guests: arr});
  },
  /**
   * Removed the item at the given index.
   *
   * @param {number} i - index in the internal array to to find the object
   *                     to remove
   */
  remove: function(index) {
    var arr = this.state.guests;
    arr.splice(index, 1);
    this.setState({guests: arr});
  },
  /**
   * Utility method to create a GuestListItem component from each object in the
   * array.
   *
   * @param {object} guest - object associated with GuestListItem; contains
   *                         everything plus the ID
   * @param {number} i - index for the object to generate the component
   */
  eachGuest: function(guest, i) {
    var guest = this.state.guests[i];
    /* the component below is given the functions this.update and this.remove so
     * that it has a means of updating its own associated object in this
     * component
     */
    return (
      <GuestListItem
        key={i}
        index={i}
        onChange={this.update}
        onRemove={this.remove}
        name={guest.name}
        email={guest.email}
        status={guest.status}
      />
    );
  },

  /**
   * Generates the table.
   */
  render: function() {
    console.table(this.state.guests);
    return (
    <div>
      <table className="table table-hover table-condensed">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Attending?</th>
          <th/>
        </tr>
      </thead>
      <tbody>
        {this.state.guests.map(this.eachGuest)}
      </tbody>
      </table>
      <button onClick={this.add}
      className="btn btn-primary">
        <span className="glyphicon glyphicon-plus"/>
        <span> Add New Guest</span>
      </button>
      {/* If content is entered into the body of the component, it'd show up here */}
      <p>
        {this.props.children}
      </p>
    </div>
  );}
});

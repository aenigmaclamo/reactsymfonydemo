# Requirements
You should get react-tools from npm if you don't already have it:

    $ sudo npm install -g react-tools

# Compiling jsx
For development I did this:

    $ jsx -w jsx/ js/

This will watch for changes and reflect them in the output directory.

If you mess something up, there will be a slew of errors in this
console screen.

# Finding React Stuff
Go to [src/SiliconTravel/ItineraryBuilder/GuestListBundle/Resources/public/](src/SiliconTravel/ItineraryBuilder/GuestListBundle/Resources/public/).
